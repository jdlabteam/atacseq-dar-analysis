#!/bin/bash 
#SBATCH --cpus-per-task=6
#SBATCH --mem=50G 
#SBATCH -J shift 
#SBATCH --array=1-12
#SBATCH -o /working directory/logs/shift.out 
#SBATCH -e /working directory/logs/shift.err

module load deeptools
module load samtools

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 

samtools index -b /working directory/results/after_filter/${ID}.filtered.bam
alignmentSieve --ATACshift --bam /working directory/results/after_filter/${ID}.filtered.bam -o /working directory/results/after_filter/${ID}.shift.temp.bam
samtools sort -O bam -o /working directory/results/after_filter/${ID}.shifted.bam /working directory/results/after_filter/${ID}.shift.temp.bam
samtools index -b /working directory/results/after_filter/${ID}.shifted.bam
rm /working directory/results/after_filter/${ID}.shift.temp.bam