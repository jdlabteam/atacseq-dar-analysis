#!/bin/bash 
#SBATCH --cpus-per-task=6
#SBATCH --mem=50G 
#SBATCH -J bowtie_align
#SBATCH --array=1-12
#SBATCH -o /working directory/logs/bowtie2_align.out 
#SBATCH -e /working directory/logs/bowtie2_align.err 

module load bowtie2 
module load samtools

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 

bowtie2 -x /working directory/build_bowtie2_index/mouse_bowtie2_index/mouse_bowtie2_index \
        --local --very-sensitive-local --no-unal --no-mixed --no-discordan --phred33 \
        -I 10 -X 2000 -p2 -1 /working directory/results/trimmed/${ID}_trimmed_1P.fastq.gz -2 /working directory/results/trimmed/${ID}_trimmed_2P.fastq.gz \
        -S /working directory/results/post_bowtie2_align/${ID}.sam
        
samtools sort /working directory/results/post_bowtie2_align/${ID}.sam -O bam -o /working directory/results/post_bowtie2_align/${ID}.sorted.bam
samtools index -b /working directory/results/post_bowtie2_align/${ID}.sorted.bam