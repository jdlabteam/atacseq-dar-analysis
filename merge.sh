#!/bin/bash 
#SBATCH --cpus-per-task=6
#SBATCH --mem=50G 
#SBATCH -J merge_bam 
#SBATCH --array=1
#SBATCH -o /working directory/logs/merge_all.out 
#SBATCH -e /working directory/logs/merge_all.err

module load samtools

samtools merge /working directory/results/merged_bam/all.merged.bam /working directory/results/after_filter/*.bam