## Set Working Directory
setwd("A:/Dougherty Lab/Myt1L/RNAseq/PFC RNAseq/results")
## Load required library
library("edgeR")
library("biomaRt")
library("ggplot2")
library("data.table")
library("combinat")
library("gridExtra")
library("gplots")
library("plyr")
library("Rtsne")
library("stringr")
library("ggforce")
library("readr")
library("dplyr")
library("ggpubr")
library("tidyverse")
library("VennDiagram")
library("Hmisc")
library("corrplot")
library("reshape2")
library("ggrepel")
library("DESeq2")
library("RUVSeq")

## Read peak counts and generate dge object 
files <- dir(path = "counts", pattern = "*counts.txt$")
counts <- readDGE(files, path = "counts", header=F)$counts
## Read sample information
samples<-read.csv("DAR analysis/samples.csv")
colnames(counts)<-samples$sample
dge <- DGEList(counts = counts, 
               group = samples$group,
               genes = rownames(counts)
)
dge$samples$genotype <- samples$genotype
dge$samples$sex <- samples$sex
dge$samples$group <- samples$group
## Filter out the low counts peaks
keep<-filterByExpr(dge)
table(keep)
dge <- dge[keep, , keep.lib.sizes=FALSE]
## normalizes for RNA composition
dge <- calcNormFactors(dge, method = "upperquartile")
saveRDS(dge, "DAR analysis/results/objs/dge_filt_norm.rds")

## Read dge object and generate log2(cpm) table
dge <- readRDS("DAR analysis/results/objs/dge_filt_norm.rds")
# get the log2(cpm) 
cpm <- data.table(dge$genes, cpm(dge, log = T))
write.csv(cpm, "DAR analysis/results/reports/cpm.csv", row.names = F)
dge$samples$group <- factor(dge$samples$group, levels = c("WT","Het","Hom"))

##Find DAR by edgeR
#Design the matrix by genotype
group<- factor(samples$genotype, levels = c("WT", "Het","Hom"))
design<- model.matrix(~group)
# Explore data: BCV plot (Biological coefficient of variation)
dge<- estimateDisp(dge, design)
#Check the common BCV value
message("common dispersion = ", sqrt(dge$common.dispersion))
pdf("DAR analysis/results/figs/Before_RUV_BCV.pdf")
plotBCV(dge)
dev.off()
# Remove unwanted variation using Residuals
fit <- glmFit(dge, design)
res <- residuals(fit, type="deviance")
countsUQ<-betweenLaneNormalization(dge$counts, which="upper")
controls<-rownames(dge$counts)
counts_RUVr<-RUVr(countsUQ, controls, k=3, res)
counts_after_RUV<-counts_RUVr$normalizedCounts
# Get DARs before RUVr normalization
fit_de_Het <- glmLRT(fit, coef = 2)
fit_de_Hom <- glmLRT(fit, coef = 3)
fit_de_geno <- glmLRT(fit, coef = 2:3)
de_Het<-as.data.frame(topTags(fit_de_Het, n=Inf, sort.by = "PValue"))
de_Hom<-as.data.frame(topTags(fit_de_Hom, n=Inf, sort.by = "PValue"))
de_geno<-as.data.frame(topTags(fit_de_geno, n=Inf, sort.by = "PValue"))
write.csv(de_Het, "DAR analysis/results/reports/DAR_Het-WT_before_RUVr.csv")
write.csv(de_Hom, "DAR analysis/results/reports/DAR_Hom-WT_before_RUVr.csv")
write.csv(de_geno, "DAR analysis/results/reports/DAR_geno_before_RUVr.csv")

## Get DARs after RUVr normalization
dge_RUVr <- DGEList(counts = counts_after_RUV, 
               group = samples$genotype,
               genes = rownames(counts_after_RUV)
)
dge_RUVr$samples$group <- samples$genotype
# normalizes for RNA composition
dge_RUVr <- calcNormFactors(dge_RUVr, method = "upperquartile")
saveRDS(dge_RUVr, "DAR analysis/results/objs/dge_filt_norm_after_RUVr.rds")
# get the log2(cpm) 
cpm_after_RUVr<- data.table(dge_RUVr$genes, cpm(dge_RUVr, log = T))
write.csv(cpm_after_RUVr, "DAR analysis/results/reports/cpm_after_RUVr.csv", row.names = F)
dge_RUVr$samples$group <- factor(dge_RUVr$samples$group, levels = c("WT","Het","Hom"))
# Explore data: BCV plot (Biological coefficient of variation)
dge_RUVr<- estimateDisp(dge_RUVr, design)
# Check the common BCV value
message("common dispersion = ", sqrt(dge_RUVr$common.dispersion))
pdf("DAR analysis/results/figs/After_RUV_BCV.pdf")
plotBCV(dge_RUVr)
dev.off()
# Find DARs by glmFit
fit_RUVr <- glmFit(dge_RUVr, design)
fit_de_RUVr_Het <- glmLRT(fit_RUVr, coef = 2)
fit_de_RUVr_Hom <- glmLRT(fit_RUVr, coef = 3)
fit_de_RUVr_geno <- glmLRT(fit_RUVr, coef = 2:3)
de_RUVr_Het<-as.data.frame(topTags(fit_de_RUVr_Het, n=Inf, sort.by = "PValue"))
de_RUVr_Hom<-as.data.frame(topTags(fit_de_RUVr_Hom, n=Inf, sort.by = "PValue"))
de_RUVr_geno<-as.data.frame(topTags(fit_de_RUVr_geno, n=Inf, sort.by = "PValue"))
write.csv(de_RUVr_Het, "DAR analysis/results/reports/DAR_Het-WT_after_RUVr.csv")
write.csv(de_RUVr_Hom, "DAR analysis/results/reports/DAR_Hom-WT_after_RUVr.csv")
write.csv(de_RUVr_geno, "DAR analysis/results/reports/DAR_geno_after_RUVr.csv")
