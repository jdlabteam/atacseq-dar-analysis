#!/bin/bash 
#SBATCH --cpus-per-task=6
#SBATCH --mem=100G 
#SBATCH -J macs2 
#SBATCH --array=1
#SBATCH -o /working directory/logs/macs2_all.out 
#SBATCH -e /working directory/logs/macs2_all.err

module load macs2

macs2 callpeak -f BAMPE -g mm --keep-dup all --cutoff-analysis -n all \
  -t /working directory/results/merged_bam/all.merged.bam \
  --outdir /working directory/results/peak_calling 2> /working directory/results/peak_calling/all_macs2.log
